<?php

namespace App\Controller;

use App\Entity\StatHeroEntryValue;
use App\Repository\HeroRepository;
use App\Repository\StatEntryItemTypeRepository;
use App\Repository\StatHeroEntryValueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param HeroRepository $heroRepository
     * @param StatEntryItemTypeRepository $statEntryItemTypeRepository
     * @param StatHeroEntryValueRepository $statHeroEntryValueRepository
     * @return Response
     */
    public function index(
        Request $request,
        HeroRepository $heroRepository,
        StatEntryItemTypeRepository $statEntryItemTypeRepository,
        StatHeroEntryValueRepository $statHeroEntryValueRepository
    )
    {
        $heroes = $heroRepository->findBy([], ['title' => 'asc']);
        $statsTypes = $statEntryItemTypeRepository->findAll();
        $fieldTypes = [
            [
                'id' => 12161,
                'title' => 'healingDoneAvgPer10Min'
            ],
            [
                'id' => 12162,
                'title' => 'heroDamageDoneAvgPer10Min'
            ]
        ];

        $selectedHero = $request->get('hero');
        $selectedStatType = $request->get('statType');
        $selectedFieldType = $request->get('fieldType');
        if ($selectedFieldType != 12161 && $selectedFieldType != 12162) {
            $selectedFieldType = null;
        }

        if ($selectedHero && $selectedStatType && $selectedFieldType) {
            $hero = $heroRepository->find($selectedHero);
        }

        $data = [];
        if ($selectedHero && $selectedStatType && $selectedFieldType) {
            $data = $this->getData($selectedHero, $selectedStatType, $statHeroEntryValueRepository, $selectedFieldType, $hero->getRole());
            $data = $this->composeData($data);
            $data = $this->calculateAverages($data);
            ksort($data, SORT_NATURAL);
        }

        $totalEntries = 0;
        foreach ($data as $item) {
            $totalEntries += $item['entries'];
        }

        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
            'content' => $data,
            'heroes' => $heroes,
            'selectedHero' => $selectedHero,
            'statTypes' => $statsTypes,
            'selectedStatType' => $selectedStatType,
            'fieldTypes' => $fieldTypes,
            'selectedFieldType' => $selectedFieldType,
            'totalEntries' => $totalEntries,
        ]);
    }

    private function getData($selectedHero, $selectedStatType, $statHeroEntryValueRepository, $selectedFieldType, $heroType)
    {
        $src = $statHeroEntryValueRepository->findByHeroAndStatType($selectedHero, $selectedStatType, $selectedFieldType, $heroType);
        $data = [];
        foreach ($src as $item) {
            $time = $statHeroEntryValueRepository->findOneBy([
                'statEntryItem' => $item->getStatEntryItem()->getId(),
                'statHeroEntryFieldType' => '12196'
            ]);
            $data[] = [
                'id' => $item->getId(),
                'field_name' => $item->getStatHeroEntryFieldType()->getTitle(),
                'field_value' => $item->getValue(),
                'sr' => $item->getStatEntryItem()->getStatEntry()->getSupportLevel(),
                'time' => $time ? (int)$time->getValue() : 0,
            ];

            $data = array_filter($data, function($item) { return $item['time'] >= 3600; });
        }

        return $data;
    }

    private function composeData(array $data)
    {
        $ret = [];

        foreach ($data as $item) {
            $srBegin = 100 * intdiv($item['sr'], 100);
            $srEnd = $srBegin == 0 ? 499 : $srBegin + 100 - 1;
            $srRange = "$srBegin - $srEnd";
            if (!array_key_exists($srRange, $ret)) {
                $ret[$srRange] = [
                    'value' => 0,
                    'time' => 0,
                    'entries' => 0
                ];
            }

            $ret[$srRange]['entries']++;
            $ret[$srRange]['time'] += $item['time'];
            $ret[$srRange]['value'] += (float)$item['field_value'];
        }

        return $ret;
    }

    private function calculateAverages(array $data)
    {
        foreach ($data as &$range) {
            $range['avg_value'] = number_format($range['value']/$range['entries'], 0, '.', ' ');
            $range['time'] = $this->prettyTime($range['time']);
        }

        return $data;
    }

    private function prettyTime(int $time)
    {
        $h = intdiv($time, 3600);
        $time -= 3600 * $h;
        $m = intdiv($time, 60);

        return "{$h}h {$m}min";
    }


}
