<?php
/**
 * Created by Łukasz Wołczak <lukasz.wolczak@gmail.com>
 * Date: 2020-06-11
 */

namespace App\Service\Exception;

use Exception;
use Throwable;

class StatsProcessorDataException extends Exception
{
    public function __construct(array $fields, $code = 0, Throwable $previous = null)
    {
        $message = "Unknown fields: " . json_encode($fields);
        parent::__construct($message, $code, $previous);
    }

}