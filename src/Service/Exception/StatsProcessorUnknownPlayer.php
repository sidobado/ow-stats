<?php
/**
 * Created by Łukasz Wołczak <lukasz.wolczak@gmail.com>
 * Date: 2020-06-11
 */

namespace App\Service\Exception;

use Exception;
use Throwable;

class StatsProcessorUnknownPlayer extends Exception
{
    public function __construct(string $player, $code = 0, Throwable $previous = null)
    {
        $message = "Unknown player: $player";
        parent::__construct($message, $code, $previous);
    }

}