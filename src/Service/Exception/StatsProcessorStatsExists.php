<?php
/**
 * Created by Łukasz Wołczak <lukasz.wolczak@gmail.com>
 * Date: 2020-06-11
 */

namespace App\Service\Exception;

use Exception;
use Throwable;

class StatsProcessorStatsExists extends Exception
{
    public function __construct($code = 0, Throwable $previous = null)
    {
        $message = "Stats exists";
        parent::__construct($message, $code, $previous);
    }

}