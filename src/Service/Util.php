<?php
/**
 * Created by Łukasz Wołczak <lukasz.wolczak@gmail.com>
 * Date: 2020-06-07
 */

declare(strict_types=1);

namespace App\Service;

use App\Entity\StatsQueue;

class Util
{
    public static function GetHashForStatsQueue(StatsQueue $statsQueue)
    {
        return hash('sha512', json_encode($statsQueue->getData()));
    }

    public static function GetHashForArray(array $arr)
    {
        return hash('sha512', json_encode($arr));
    }
}