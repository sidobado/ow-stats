<?php
/**
 * Created by Łukasz Wołczak <lukasz.wolczak@gmail.com>
 * Date: 2020-06-06
 */

declare(strict_types=1);

namespace App\Service;

use App\Entity\Hero;
use App\Entity\StatEntry;
use App\Entity\StatEntryItem;
use App\Entity\StatEntryItemType;
use App\Entity\StatHeroEntryFieldType;
use App\Entity\StatHeroEntryGroup;
use App\Entity\StatHeroEntryValue;
use App\Entity\StatsQueue;
use App\Repository\HeroRepository;
use App\Repository\PlayerRepository;
use App\Repository\StatEntryItemTypeRepository;
use App\Repository\StatEntryRepository;
use App\Repository\StatHeroEntryFieldTypeRepository;
use App\Repository\StatHeroEntryGroupRepository;
use App\Service\Exception\StatsProcessorDataException;
use App\Service\Exception\StatsProcessorStatsExists;
use App\Service\Exception\StatsProcessorUnknownPlayer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class StatsProcessor
{
    /**
     * @var StatsQueue
     */
    private $stats;
    /**
     * @var StatEntryRepository
     */
    private $statEntryRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var StatEntry
     */
    private $statEntry;
    /**
     * @var array|null
     */
    private $statsData;
    /**
     * @var PlayerRepository
     */
    private $playerRepository;
    /**
     * @var StatEntryItemTypeRepository
     */
    private $statEntryItemTypeRepository;
    /**
     * @var HeroRepository
     */
    private $heroRepository;
    /**
     * @var StatHeroEntryGroupRepository
     */
    private $statHeroEntryGroupRepository;
    /**
     * @var StatHeroEntryFieldTypeRepository
     */
    private $statHeroEntryFieldTypeRepository;

    /**
     * StatsProcessor constructor.
     * @param StatsQueue $stats
     * @param StatEntryRepository $statEntryRepository
     * @param PlayerRepository $playerRepository
     * @param StatEntryItemTypeRepository $statEntryItemTypeRepository
     * @param HeroRepository $heroRepository
     * @param StatHeroEntryGroupRepository $statHeroEntryGroupRepository
     * @param StatHeroEntryFieldTypeRepository $statHeroEntryFieldTypeRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        StatsQueue $stats,
        StatEntryRepository $statEntryRepository,
        PlayerRepository $playerRepository,
        StatEntryItemTypeRepository $statEntryItemTypeRepository,
        HeroRepository $heroRepository,
        StatHeroEntryGroupRepository $statHeroEntryGroupRepository,
        StatHeroEntryFieldTypeRepository $statHeroEntryFieldTypeRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->stats = $stats;
        $this->statsData = $this->stats->getData();
        $this->statEntryRepository = $statEntryRepository;
        $this->playerRepository = $playerRepository;
        $this->statEntryItemTypeRepository = $statEntryItemTypeRepository;
        $this->heroRepository = $heroRepository;
        $this->entityManager = $entityManager;

        $this->statEntry = new StatEntry();
        $this->statHeroEntryGroupRepository = $statHeroEntryGroupRepository;
        $this->statHeroEntryFieldTypeRepository = $statHeroEntryFieldTypeRepository;
    }

    public function execute()
    {
        try {
            $this->process();
        } catch (StatsProcessorDataException $e) {
            var_dump($e->getMessage());
            die;
//            $this->processSuccess();
        } catch (StatsProcessorStatsExists $e) {
            $this->processSuccess();
            $this->processFinish();
            return;
        } catch (StatsProcessorUnknownPlayer $e) {
            $this->processSuccess();
            $this->processFinish();
            return;
        }

        $this->processSuccess();
        $this->processFinish();
    }

    /**
     * @throws StatsProcessorDataException
     * @throws StatsProcessorStatsExists
     * @throws StatsProcessorUnknownPlayer
     */
    private function process()
    {
        $hashCode = Util::GetHashForStatsQueue($this->stats);
        $newestStat = $this->statEntryRepository->findOneBy(['hashCode' => $hashCode]);
        if ($newestStat) {
            throw new StatsProcessorStatsExists();
        }

        if (isset($this->statsData['error'])) {
            $messages = [
                "Failed to decode platform API response: invalid character '<' looking for beginning of value",
            ];
            if (in_array($this->statsData['error'], $messages)) {
                return;
            }
        }

        $this->processAccount();

        if ($this->statEntry->getPlayer() === null) {
            throw new StatsProcessorUnknownPlayer($this->stats->getData()['name']);
        }

        $this->statEntry->setHashCode($hashCode);
        $this->statEntry->setQuickPlayStatsHashCode(Util::GetHashForArray($this->stats->getData()['competitiveStats'] ?? []));
        $this->statEntry->setCompetitiveStatsHashCode(Util::GetHashForArray($this->stats->getData()['quickPlayStats'] ?? []));
        $this->processFlatElements();
        $this->processRatings();
        $this->entityManager->persist($this->statEntry);

        $this->processStatEntryItems();

        if ($this->statsData) {
            throw new StatsProcessorDataException($this->statsData);
        }

        $player = $this->statEntry->getPlayer();
        $player->setLastUpdate(new DateTime());
        $this->entityManager->persist($player);
    }

    private function processAccount()
    {
        $name = $this->statsData['name'];
        $player = $this->playerRepository->findOneBy(['name' => $name]);
        $this->statEntry->setPlayer($player);
        unset($this->statsData['name']);
    }

    private function processFlatElements()
    {
        $this->setOnEntry('endorsement');
        $this->setOnEntry('endorsementIcon');
        $this->setOnEntry('gamesWon');
        $this->setOnEntry('icon');
        $this->setOnEntry('level');
        $this->setOnEntry('levelIcon');
        $this->setOnEntry('prestige');
        $this->setOnEntry('prestigeIcon');
        $this->setOnEntry('private');
        $this->setOnEntry('rating');
        $this->setOnEntry('ratingIcon');
    }

    private function setOnEntry(string $key)
    {
        $value = $this->statsData[$key] ?? null;
        unset($this->statsData[$key]);

        $method = "set" . ucfirst($key);
        $this->statEntry->$method($value);
    }

    private function processRatings()
    {
        $tank = $this->getRatings('tank');
        $this->statEntry->setTankLevel($tank['level'] ?? null);
        $this->statEntry->setTankRankIcon($tank['rankIcon'] ?? null);
        $this->statEntry->setTankRoleIcon($tank['roleIcon'] ?? null);

        $damage = $this->getRatings('damage');
        $this->statEntry->setDamageLevel($damage['level'] ?? null);
        $this->statEntry->setDamageRankIcon($damage['rankIcon'] ?? null);
        $this->statEntry->setDamageRoleIcon($damage['roleIcon'] ?? null);

        $support = $this->getRatings('support');
        $this->statEntry->setSupportLevel($support['level'] ?? null);
        $this->statEntry->setSupportRankIcon($support['rankIcon'] ?? null);
        $this->statEntry->setSupportRoleIcon($support['roleIcon'] ?? null);

        unset($this->statsData['ratings']);
    }

    private function getRatings(string $role): ?array
    {
        $ratings = $this->statsData['ratings'] ?? [];
        foreach ($ratings as $rating) {
            if ($rating['role'] === $role) {
                return $rating;
            }
        }

        return null;
    }

    private function processStatEntryItems()
    {
        $types = $this->statEntryItemTypeRepository->findAll();
        foreach ($types as $type) {
            $this->processStatEntryItemType($type);
            unset($this->statsData[$type->getCode()]['topHeroes']);
            if (empty($this->statsData[$type->getCode()])) {
                unset($this->statsData[$type->getCode()]);
            }
        }
    }

    private function processStatEntryItemType(StatEntryItemType $type)
    {
        $statEntryItem = new StatEntryItem();
        $statEntryItem->setStatEntry($this->statEntry);
        $statEntryItem->setStatEntryItemType($type);

        $this->setEntryItemData($type, $statEntryItem);
    }

    private function setEntryItemData(StatEntryItemType $type, StatEntryItem $statEntryItem)
    {
        unset ($this->statsData[$type->getCode()]['topHeroes']);
        $fields = [
            'awards' => ['cards', 'medals', 'medalsBronze', 'medalsSilver', 'medalsGold'],
            'games' => ['played', 'won']
        ];

        foreach ($fields as $group => $fieldNames) {
            foreach ($fieldNames as $fieldName) {
                $this->setEntryItemDataField($type, $statEntryItem, $group, $fieldName);
            }
            if (empty($this->statsData[$type->getCode()][$group])) {
                unset($this->statsData[$type->getCode()][$group]);
            }
        }
        $this->entityManager->persist($statEntryItem);
        $this->processCareerStats($type, $statEntryItem);
    }

    private function setEntryItemDataField(StatEntryItemType $type, StatEntryItem $statEntryItem, string $group, string $fieldName)
    {
        $method = "set" . ucfirst($fieldName);
        $value = $this->statsData[$type->getCode()][$group][$fieldName] ?? null;
        unset($this->statsData[$type->getCode()][$group][$fieldName]);

        $statEntryItem->$method($value);
    }

    private function processCareerStats(StatEntryItemType $type, StatEntryItem $statEntryItem)
    {
        $arr = $this->statsData[$type->getCode()]['careerStats'] ?? [];
        foreach ($arr as $heroCode => $data) {
            $this->processHeroGroupEntry($type, $statEntryItem, $heroCode, $data);
            if (empty($this->statsData[$type->getCode()]['careerStats'][$heroCode])) {
                unset($this->statsData[$type->getCode()]['careerStats'][$heroCode]);
            }
        }

        if (empty($this->statsData[$type->getCode()]['careerStats'])) {
            unset($this->statsData[$type->getCode()]['careerStats']);
        }
    }

    private function processHeroGroupEntry(StatEntryItemType $type, StatEntryItem $statEntryItem, string $heroCode, $data)
    {
        $hero = $this->findOrCreateHero($heroCode);
        foreach ($data as $groupCode => $fields) {
            $this->processHeroItems($type, $statEntryItem, $hero, $groupCode, $fields);
        }
    }

    private function findOrCreateHero(string $heroCode)
    {
        static $cache = [];
        $key = $heroCode;
        if (isset($cache[$key])) {
            return $cache[$key];
        }
        $hero = $this->heroRepository->findOneBy(['code' => $heroCode]);
        if ($hero) {
            $cache[$key] = $hero;
            return $hero;
        }

        $hero = new Hero();
        $hero->setCode($heroCode);
        $hero->setTitle($heroCode);
        $this->entityManager->persist($hero);

        $cache[$key] = $hero;

        return $hero;
    }

    private function processHeroItems(StatEntryItemType $type, StatEntryItem $statEntryItem, Hero $hero, string $groupCode, $fields)
    {
        if (empty($this->statsData[$type->getCode()]['careerStats'][$hero->getCode()][$groupCode])) {
            unset($this->statsData[$type->getCode()]['careerStats'][$hero->getCode()][$groupCode]);
            return;
        }

        $group = $this->findOrCreateHeroStatGroup($groupCode);
        foreach ($this->statsData[$type->getCode()]['careerStats'][$hero->getCode()][$groupCode] as $fieldCode => $value) {
            $fieldType = $this->findOrCreateFieldType($group, $fieldCode, $value);

            $this->createNewHeroStatValue(
                $statEntryItem,
                $hero,
                $fieldType,
                $value
            );

            unset($this->statsData[$type->getCode()]['careerStats'][$hero->getCode()][$groupCode][$fieldCode]);
        }

        if (empty($this->statsData[$type->getCode()]['careerStats'][$hero->getCode()][$groupCode])) {
            unset($this->statsData[$type->getCode()]['careerStats'][$hero->getCode()][$groupCode]);
            return;
        }
    }

    private function findOrCreateHeroStatGroup(string $groupCode)
    {
        static $cache = [];
        $key = $groupCode;
        if (isset($cache[$key])) {
            return $cache[$key];
        }
        $group = $this->statHeroEntryGroupRepository->findOneBy(['code' => $groupCode]);
        if ($group) {
            $cache[$key] = $group;
            return $group;
        }

        $group = new StatHeroEntryGroup();
        $group->setTitle($groupCode);
        $group->setCode($groupCode);
        $this->entityManager->persist($group);

        $cache[$key] = $group;

        return $group;
    }

    private function findOrCreateFieldType(StatHeroEntryGroup $group, string $fieldCode, $value)
    {
        static $cache = [];
        $key = $group->getCode() . ':::' . $fieldCode;
        if (isset($cache[$key])) {
            return $cache[$key];
        }
        $fieldType = $this->statHeroEntryFieldTypeRepository->findOneBy(['code' => $fieldCode]);
        if ($fieldType) {
            $cache[$key] = $fieldType;
            return $fieldType;
        }

        $valueType = $this->determineValueType($value);
        $fieldType = new StatHeroEntryFieldType();
        $fieldType->setCode($fieldCode);
        $fieldType->setStatHeroEntryGroup($group);
        $fieldType->setValueType($valueType);
        $fieldType->setTitle($fieldCode);
        $this->entityManager->persist($fieldType);

        $cache[$key] = $fieldType;

        return $fieldType;
    }

    private function determineValueType($value)
    {
        if (strpos((string)$value, '%') !== false) {
            return 'percent';
        } elseif (strpos((string)$value, ':') !== false) {
            return 'time';
        } else {
            return 'number';
        }
    }

    private function createNewHeroStatValue(StatEntryItem $statEntryItem, Hero $hero, StatHeroEntryFieldType $fieldType, $value)
    {
        $insert = new StatHeroEntryValue();
        $insert->setHero($hero);
        $insert->setStatEntryItem($statEntryItem);
        $insert->setStatHeroEntryGroup($fieldType->getStatHeroEntryGroup());
        $insert->setStatHeroEntryFieldType($fieldType);
        $insert->setValueType($fieldType->getValueType());

        $insertValue = null;
        switch ($fieldType->getValueType()) {
            case 'percent':
                $insertValue = str_replace("%", "", $value);
                $insertValue = ((float)$insertValue) / 100;
                break;
            case 'number':
                $insertValue = $value;
                break;
            case 'time':
                if (count(explode(':', $value)) === 2) {
                    $value = '0:' . $value;
                }
                $parts = explode(':', $value);
                $insertValue = $parts[2];
                $insertValue += 60 * $parts[1];
                $insertValue += 3600 * $parts[0];
                break;
        }

        if ($insertValue === null) {
            die("\nNULL VALUE\n");
        }

        $insert->setValue((string)$insertValue);//        $this->entityManager->flush();

        $this->entityManager->persist($insert);
    }

    private function processSuccess()
    {
        $this->entityManager->remove($this->stats);
    }

    private function processFinish()
    {
        $this->entityManager->flush();
        echo ".";
    }


}