<?php
/**
 * Created by Łukasz Wołczak <lukasz.wolczak@gmail.com>
 * Date: 2020-05-06
 */

declare(strict_types=1);

namespace App\POC;

use DateTimeInterface;

class PlayerCard
{
    /** @var int */
    protected $playerLevel = '';
    /** @var string */
    protected $playerIcon = '';
    /** @var string */
    protected $playerName = '';
    /** @var DateTimeInterface */
    protected $lastUpdate;
    /** @var string */
    protected $platform = '';
    /** @var int */
    protected $sr = 0;
    /** @var string[] */
    protected $topHeroes = [];
    /** @var bool */
    protected $outdated = false;

    /**
     * @return int
     */
    public function getPlayerLevel(): int
    {
        return $this->playerLevel;
    }

    /**
     * @param int $playerLevel
     * @return PlayerCard
     */
    public function setPlayerLevel(int $playerLevel): PlayerCard
    {
        $this->playerLevel = $playerLevel;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlayerIcon(): string
    {
        return $this->playerIcon;
    }

    /**
     * @param string $playerIcon
     * @return PlayerCard
     */
    public function setPlayerIcon(string $playerIcon): PlayerCard
    {
        $this->playerIcon = $playerIcon;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    /**
     * @param string $playerName
     * @return PlayerCard
     */
    public function setPlayerName(string $playerName): PlayerCard
    {
        $this->playerName = $playerName;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getLastUpdate(): DateTimeInterface
    {
        return $this->lastUpdate;
    }

    /**
     * @param DateTimeInterface $lastUpdate
     * @return PlayerCard
     */
    public function setLastUpdate(DateTimeInterface $lastUpdate): PlayerCard
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     * @return PlayerCard
     */
    public function setPlatform(string $platform): PlayerCard
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return int
     */
    public function getSr(): int
    {
        return $this->sr;
    }

    /**
     * @param int $sr
     * @return PlayerCard
     */
    public function setSr(int $sr): PlayerCard
    {
        $this->sr = $sr;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getTopHeroes(): array
    {
        return $this->topHeroes;
    }

    /**
     * @param string[] $topHeroes
     * @return PlayerCard
     */
    public function setTopHeroes(array $topHeroes): PlayerCard
    {
        $this->topHeroes = $topHeroes;
        return $this;
    }

    /**
     * @param string $hero
     * @return PlayerCard
     */
    public function addTopHero(string $hero): PlayerCard
    {
        $this->topHeroes[] = $hero;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOutdated(): bool
    {
        return $this->outdated;
    }

    /**
     * @param bool $outdated
     * @return PlayerCard
     */
    public function setOutdated(bool $outdated): PlayerCard
    {
        $this->outdated = $outdated;
        return $this;
    }

    public function validate()
    {
        $vars = get_object_vars($this);
        $errors = [];
        foreach ($vars as $key => $val) {
            if (!$this->{$key}) {
                $errors[] = $key;
            }
        }

        var_dump($errors);
    }
}