<?php

namespace App\Command;

use App\Entity\BattleTagNumber;
use App\Entity\PlayerCard;
use App\Repository\BattleTagNumberRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Panther\Client;
use Symfony\Component\Panther\DomCrawler\Crawler;

class DownloadPlayerCardsCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'app:download_player_cards';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var BattleTagNumberRepository
     */
    private $battleTagNumberRepository;

    /**
     * DownloadPlayerCardsCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     * @param BattleTagNumberRepository $battleTagNumberRepository
     */
    public function __construct(string $name = null, EntityManagerInterface $entityManager, BattleTagNumberRepository $battleTagNumberRepository)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->battleTagNumberRepository = $battleTagNumberRepository;
    }


    protected function configure()
    {
        $this
            ->setDescription('Fills database with player cards');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            return -1;
        }

        while ($numbers = $this->battleTagNumberRepository->getRandomNumber()) {
            foreach ($numbers as $number) {
                try {
                    $playerCards = $this->downloadPlayers($number);
                } catch (NoSuchElementException $e) {
                    $this->log("------------------------------------------------------");
                    $this->log("------------------------------------------------------");
                    $this->log(json_encode($e, JSON_PRETTY_PRINT) . "\n");
                    $this->log("------------------------------------------------------");
                    $this->log("------------------------------------------------------");
                    break;
                }
                $number->setHandled(true);
                $this->log("For query: '" . $number->getNumber() . "' downloaded '" . count($playerCards) . "' players.\n");
                $this->entityManager->persist($number);

                foreach ($playerCards as $playerCard) {
                    $this->entityManager->persist($playerCard);
                }
            }
            $this->entityManager->flush();
        }

        $this->release();
        return 0;
    }

    /**
     * @param BattleTagNumber $number
     * @return PlayerCard[]
     * @throws NoSuchElementException
     */
    private function downloadPlayers(BattleTagNumber $number): array
    {
        $options = [
            '--headless',
            '--no-sandbox',
            '--remote-debugging-port=9222'
        ];
        $arr = [];
        $client = Client::createChromeClient(null, $options);
        $url = 'https://www.overbuff.com/search?q=%23' . $number->getNumber();
        $client->request('GET', $url);
        try {
            $crawler = $client->waitFor('.SearchResult');
        } catch (TimeoutException $e) {
            echo "Timeout: $url\n";
            $this->log("Timeout url: $url\n");
            $this->log("Timeout number: " . $number->getNumber() . "\n");
            return [];
        }
        if ($crawler->filter('.SearchResult')->count() <= 0) {
            var_dump($number); die;
            return $arr;
        }
        $crawler->filter('.SearchResult')->each(function(Crawler $item) use (&$arr) {
            $card = new PlayerCard();

            $card->setOutdated($item->filter('.fa-exclamation-triangle')->count() > 0);

            if ($item->filter('.player-name-wrapper .smaller time')->count()) {
                $card->setLastUpdate(new DateTime($item->filter('.player-name-wrapper .smaller time')->getAttribute('datetime')));
            }

            if ($item->filter('.player-platform-icon i')->count() > 0) {
                $card->setPlatform($item->filter('.player-platform-icon i')->getAttribute('class'));
            } else {
                $card->setPlatform('');
            }

            if ($item->filter('.player-name-wrapper h2')->count() > 0) {
                $card->setPlayerName($item->filter('.player-name-wrapper h2')->getText());
            } else {
                $card->setPlayerName('');
            }

            if ($item->filter('.image-player')->count() > 0) {
                $card->setPlayerIcon($item->filter('.image-player')->getAttribute('src'));
            } else {
                $card->setPlayerIcon('');
            }

            if ($item->filter('.player-level')->count() > 0) {
                $card->setPlayerLevel((int)$item->filter('.player-level')->getText());
            } else {
                $card->setPlayerLevel(0);
            }

            if ($item->filter('.player-skill-rating')->count() > 0) {
                $card->setSr((int)$item->filter('.player-skill-rating')->getText());
            } else {
                $card->setSr(0);
            }

            $item->filter('.result-heroes .hero')->each(function(Crawler $crawler) use ($card) {
                $card->addTopHero($crawler->filter('img')->getAttribute('alt'));
            });

            $card->setCreateTime(new DateTime());
            $card->setUpdateTime(new DateTime());
            $card->setVersionNumber(1);

            $arr[] = $card;
        });

        return $arr;
    }

    private function log(string $string)
    {
        $file = fopen("var/log/urls.log", "a");
        fputs($file, '[' . date('Y-m-d H:i:s') . ']' . $string);
        fclose($file);
    }


}
