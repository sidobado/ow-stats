<?php

namespace App\Command;

use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DummyCommand extends Command
{
    protected static $defaultName = 'app:dummy';

    protected function configure()
    {
        $this
            ->setDescription('Dummy command to test stuff')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $msg = sprintf("%s", (new DateTime())->format('Y-m-d H:i:sP'));

        $io->success($msg);

        return 0;
    }
}
