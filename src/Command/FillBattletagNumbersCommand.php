<?php

namespace App\Command;

use App\Entity\BattleTagNumber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FillBattletagNumbersCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'app:fill_battletag_numbers';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FillBattletagNumbersCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Fills database with numbers for future download of player cards.')
            ->addArgument('digits_count', InputArgument::REQUIRED, 'Number of digits to generate')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            return -1;
        }
        $io = new SymfonyStyle($input, $output);
        $digitsCount = $input->getArgument('digits_count');

        $this->calculateNumbers($io, (int)$digitsCount);

        $this->release();
        return 0;
    }

    private function calculateNumbers(SymfonyStyle $io, int $digitsCount, string $currentNumber = '')
    {
        $digitsCount--;
        for ($i = 0; $i <= 9; $i++) {
            $newNumber = $currentNumber . $i;
            if ($digitsCount > 0) {
                $this->calculateNumbers($io, $digitsCount, $newNumber);
            } else {
                $this->saveNumber($newNumber);
            }

            if ($digitsCount == 0 && $i == 9 && ((int)$newNumber) > 92319) {
                $this->entityManager->flush();
            }
        }

    }

    private function saveNumber(string $newNumber)
    {
        if (((int)$newNumber) <= 92319)
        {
            return;
        }

        $btNumber = new BattleTagNumber();
        $btNumber->setNumber($newNumber);
        $btNumber->setHandled(false);

        $this->entityManager->persist($btNumber);
    }
}
