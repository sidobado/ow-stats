<?php

namespace App\Command;

use App\Entity\Player;
use App\Entity\StatsQueue;
use App\Repository\PlayerRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DownloadPlayerStatisticsCommand extends Command
{
    const MAX_ITERATIONS = 100;
    protected static $defaultName = 'app:download-player-statistics';
    /**
     * @var PlayerRepository
     */
    private $playerRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, PlayerRepository $playerRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->playerRepository = $playerRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Downloads player statistics and adds them to queue for processing.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $iterations = self::MAX_ITERATIONS;

        $success = true;
        while ($iterations--) {
            $players = $this->playerRepository->findPlayersToDownloadStatistics();

            foreach ($players as $player) {
                $success = $this->processPlayer($player);
                if (false === $success) {
                    break;
                }
            }

            if (false === $success) {
                $io->error("ups, not working on player: " . $player->getName() . ' id: ' . $player->getId());
                sleep(360);
            } else {
                sleep(10);
            }
        }

        return 0;
    }

    /**
     * @param Player $player
     */
    private function processPlayer(Player $player): bool
    {
        $uri = 'https://ow-api.com/v1/stats/pc/eu/';
        $path = $player->getUrlName() . '/complete';
        $client = new Client(['base_uri' => $uri]);
        try {
            $response = $client->request('GET', $path);
        } catch (ClientException $e) {
            if ($e->getCode() === 404) {
                echo "--- deleting player " . $player->getName() . "\n";
                $this->entityManager->remove($player);
                $this->entityManager->flush();
                return true;
            }

            if ($e->getCode() === 400) {
                echo "--- bad battletag, deleting player " . $player->getName() . "\n";
                $this->entityManager->remove($player);
                $this->entityManager->flush();
                return true;
            }

            if ($e->getCode() !== 200) {
                var_dump('error on download, code: ' . $e->getCode());
                return false;
            }
        }

        if ($response->getStatusCode() !== 200) {
            return false;
        }

        return
            $this->saveStatistics($player, $response->getBody()->getContents())
            && $this->flushDb();
    }


    private function saveStatistics(Player $player, string $rawData)
    {
        $arrayData = json_decode($rawData, true);
        $stats = new StatsQueue();
        $stats->setData($arrayData);
        $stats->setHandled(false);
        $stats->setDateAdd(new DateTime());

        $player->setLastUpdate($stats->getDateAdd());

        $this->entityManager->persist($stats);
        $this->entityManager->persist($player);
        $this->entityManager->flush();
        return true;
    }


    private function flushDb()
    {
        $this->entityManager->flush();
        return true;
    }
}
