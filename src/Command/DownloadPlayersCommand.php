<?php

namespace App\Command;

use App\Entity\Player;
use App\Entity\PlayerCard;
use App\Repository\PlayerCardRepository;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadPlayersCommand extends Command
{
    protected static $defaultName = 'app:download-players';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PlayerCardRepository
     */
    private $cardRepository;
    /**
     * @var PlayerRepository
     */
    private $playerRepository;

    /**
     * DownloadPlayersCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     * @param PlayerCardRepository $cardRepository
     * @param PlayerRepository $playerRepository
     */
    public function __construct(
        string $name = null,
        EntityManagerInterface $entityManager,
        PlayerCardRepository $cardRepository,
        PlayerRepository $playerRepository
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->cardRepository = $cardRepository;
        $this->playerRepository = $playerRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Downloads players from overwatch search engine')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $card = $this->cardRepository->findCardToHandle();
        if ($card) {
            $this->processCard($card);
        }

        return 0;
    }

    private function processCard(PlayerCard $card)
    {
        $name = $this->getPlayerNameFromCard($card);
        $players = $this->downloadPlayers($name);
        echo sprintf("Found %d players\n\n\n", count($players));

        foreach ($players as $p) {
            if ($p->platform != 'pc') {
                continue;
            }
            $player = new Player();
            $player->setLevel($p->level);
            $player->setName($p->name);
            $player->setPlatform($p->platform);
            $player->setPortrait($p->portrait);
            $player->setPublic($p->isPublic);
            $player->setUrlName($p->urlName);
            $this->entityManager->persist($player);
        }

        if ($name == $card->getPlayerName()) {
            $this->cardRepository->updateHandledWithoutHash($name);
        } else {
            $this->cardRepository->updateHandled($name);
        }
        $this->entityManager->flush();
    }

    private function getPlayerNameFromCard(PlayerCard $card)
    {
        $name = explode('#', $card->getPlayerName());

        echo sprintf("Found %s, now searching for %s\n", $card->getPlayerName(), $name[0]);

        return $name[0];
    }

    private function downloadPlayers(string $name)
    {
        $data = file_get_contents("https://playoverwatch.com/en-us/search/account-by-name/" . urlencode($name));
        return json_decode($data);
    }


}
