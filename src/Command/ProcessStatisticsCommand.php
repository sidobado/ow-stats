<?php

namespace App\Command;

use App\Entity\StatsQueue;
use App\Repository\HeroRepository;
use App\Repository\PlayerRepository;
use App\Repository\StatEntryItemTypeRepository;
use App\Repository\StatEntryRepository;
use App\Repository\StatHeroEntryFieldTypeRepository;
use App\Repository\StatHeroEntryGroupRepository;
use App\Repository\StatsQueueRepository;
use App\Service\StatsProcessor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProcessStatisticsCommand extends Command
{
    protected static $defaultName = 'app:process-statistics';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var StatsQueueRepository
     */
    private $statsQueueRepository;
    /**
     * @var StatEntryRepository
     */
    private $statEntryRepository;
    /**
     * @var PlayerRepository
     */
    private $playerRepository;
    /**
     * @var StatEntryItemTypeRepository
     */
    private $statEntryItemTypeRepository;
    /**
     * @var HeroRepository
     */
    private $heroRepository;
    /**
     * @var StatHeroEntryGroupRepository
     */
    private $statHeroEntryGroupRepository;
    /**
     * @var StatHeroEntryFieldTypeRepository
     */
    private $statHeroEntryFieldTypeRepository;


    /**
     * ProcessStatisticsCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     * @param StatsQueueRepository $statsQueueRepository
     * @param StatEntryRepository $statEntryRepository
     * @param PlayerRepository $playerRepository
     * @param HeroRepository $heroRepository
     * @param StatHeroEntryGroupRepository $statHeroEntryGroupRepository
     * @param StatEntryItemTypeRepository $statEntryItemTypeRepository
     * @param StatHeroEntryFieldTypeRepository $statHeroEntryFieldTypeRepository
     */
    public function __construct(
        string $name = null,
        EntityManagerInterface $entityManager,
        StatsQueueRepository $statsQueueRepository,
        StatEntryRepository $statEntryRepository,
        PlayerRepository $playerRepository,
        HeroRepository $heroRepository,
        StatHeroEntryGroupRepository $statHeroEntryGroupRepository,
        StatEntryItemTypeRepository $statEntryItemTypeRepository,
        StatHeroEntryFieldTypeRepository $statHeroEntryFieldTypeRepository
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->statsQueueRepository = $statsQueueRepository;
        $this->statEntryRepository = $statEntryRepository;
        $this->playerRepository = $playerRepository;
        $this->statEntryItemTypeRepository = $statEntryItemTypeRepository;
        $this->heroRepository = $heroRepository;
        $this->statHeroEntryGroupRepository = $statHeroEntryGroupRepository;
        $this->statHeroEntryFieldTypeRepository = $statHeroEntryFieldTypeRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Process downloaded statistics');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $stats = $this->statsQueueRepository->findStatsToProcess();

        foreach ($stats as $stat) {
            $service = $this->getStatisticsProcessor($stat);
            $service->execute();
        }
//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }

//        $io->success('...');

        return 0;
    }

    public function getStatisticsProcessor(StatsQueue $stats): StatsProcessor
    {
        return new StatsProcessor(
            $stats,
            $this->statEntryRepository,
            $this->playerRepository,
            $this->statEntryItemTypeRepository,
            $this->heroRepository,
            $this->statHeroEntryGroupRepository,
            $this->statHeroEntryFieldTypeRepository,
            $this->entityManager
        );
    }
}
