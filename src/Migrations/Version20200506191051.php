<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200506191051 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE player_card_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql(
            'CREATE TABLE player_card (' .
                'id INT NOT NULL, ' .
                'player_level INT DEFAULT NULL, ' .
                'player_icon TEXT DEFAULT NULL, ' .
                'player_name TEXT DEFAULT NULL, ' .
                'last_update TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, ' .
                'platform TEXT DEFAULT NULL, ' .
                'sr INT DEFAULT NULL, ' .
                'top_heroes TEXT DEFAULT NULL, ' .
                'outdated BOOLEAN DEFAULT NULL, ' .
                'create_time TIMESTAMP(0) WITH TIME ZONE NOT NULL, ' .
                'update_time TIMESTAMP(0) WITH TIME ZONE NOT NULL, ' .
                'version_number INT NOT NULL, ' .
                'PRIMARY KEY(id)' .
            ')'
        );
        $this->addSql('COMMENT ON COLUMN player_card.top_heroes IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE player_card_id_seq CASCADE');
        $this->addSql('DROP TABLE player_card');
    }
}
