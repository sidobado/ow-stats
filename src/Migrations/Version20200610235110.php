<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200610235110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2020-01-01 00:00:00+02:00\'');
        $this->addSql('ALTER TABLE stat_entry DROP CONSTRAINT fk_47064066df01d6ef');
        $this->addSql('DROP INDEX idx_47064066df01d6ef');
        $this->addSql('ALTER TABLE stat_entry ADD competitive_stats_hash_code TEXT NOT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD quick_play_stats_hash_code TEXT NOT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD endorsement INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD endorsement_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD games_won INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD level INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD level_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD prestige INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD prestige_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD private BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD rating INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD rating_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD tank_level INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD tank_role_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD tank_rank_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD damage_level INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD damage_role_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD damage_rank_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD support_level INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD support_role_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD ssupport_rank_icon TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry DROP stat_entry_type_id');
        $this->addSql('ALTER TABLE stat_entry DROP cards');
        $this->addSql('ALTER TABLE stat_entry DROP medals');
        $this->addSql('ALTER TABLE stat_entry DROP medals_bronze');
        $this->addSql('ALTER TABLE stat_entry DROP medals_silver');
        $this->addSql('ALTER TABLE stat_entry DROP medals_gold');
        $this->addSql('ALTER TABLE stat_entry DROP played');
        $this->addSql('ALTER TABLE stat_entry DROP won');
        $this->addSql('ALTER TABLE stat_entry DROP top_heroes');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2019-12-31 22:00:00+00\'');
        $this->addSql('ALTER TABLE stat_entry ADD stat_entry_type_id INT NOT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD cards INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD medals INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD medals_bronze INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD medals_silver INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD medals_gold INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD played INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD won INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry ADD top_heroes JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE stat_entry DROP competitive_stats_hash_code');
        $this->addSql('ALTER TABLE stat_entry DROP quick_play_stats_hash_code');
        $this->addSql('ALTER TABLE stat_entry DROP endorsement');
        $this->addSql('ALTER TABLE stat_entry DROP endorsement_icon');
        $this->addSql('ALTER TABLE stat_entry DROP games_won');
        $this->addSql('ALTER TABLE stat_entry DROP icon');
        $this->addSql('ALTER TABLE stat_entry DROP level');
        $this->addSql('ALTER TABLE stat_entry DROP level_icon');
        $this->addSql('ALTER TABLE stat_entry DROP prestige');
        $this->addSql('ALTER TABLE stat_entry DROP prestige_icon');
        $this->addSql('ALTER TABLE stat_entry DROP private');
        $this->addSql('ALTER TABLE stat_entry DROP rating');
        $this->addSql('ALTER TABLE stat_entry DROP rating_icon');
        $this->addSql('ALTER TABLE stat_entry DROP tank_level');
        $this->addSql('ALTER TABLE stat_entry DROP tank_role_icon');
        $this->addSql('ALTER TABLE stat_entry DROP tank_rank_icon');
        $this->addSql('ALTER TABLE stat_entry DROP damage_level');
        $this->addSql('ALTER TABLE stat_entry DROP damage_role_icon');
        $this->addSql('ALTER TABLE stat_entry DROP damage_rank_icon');
        $this->addSql('ALTER TABLE stat_entry DROP support_level');
        $this->addSql('ALTER TABLE stat_entry DROP support_role_icon');
        $this->addSql('ALTER TABLE stat_entry DROP ssupport_rank_icon');
        $this->addSql('ALTER TABLE stat_entry ADD CONSTRAINT fk_47064066df01d6ef FOREIGN KEY (stat_entry_type_id) REFERENCES stat_entry_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_47064066df01d6ef ON stat_entry (stat_entry_type_id)');
    }
}
