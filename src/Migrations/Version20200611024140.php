<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200611024140 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stat_type DROP CONSTRAINT fk_ad96ac7d776308d0');
        $this->addSql('ALTER TABLE hero_stat_entry DROP CONSTRAINT fk_9dbe508321b6fb0a');
        $this->addSql('DROP SEQUENCE stat_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stat_entry_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stat_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hero_stat_entry_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE stat_entry_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stat_entry_item_type_id_seq INCREMENT BY 1 MINVALUE 1 START 3');
        $this->addSql('CREATE TABLE stat_entry_item (id INT NOT NULL, stat_entry_item_type_id INT NOT NULL, stat_entry_id INT NOT NULL, cards INT DEFAULT NULL, medals INT DEFAULT NULL, medals_bronze INT NOT NULL, medals_silver INT DEFAULT NULL, medals_gold INT DEFAULT NULL, played INT DEFAULT NULL, won INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_74ED2AA59EECAC87 ON stat_entry_item (stat_entry_item_type_id)');
        $this->addSql('CREATE INDEX IDX_74ED2AA5330198D5 ON stat_entry_item (stat_entry_id)');
        $this->addSql('CREATE TABLE stat_entry_item_type (id INT NOT NULL, code TEXT NOT NULL, title TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE stat_entry_item ADD CONSTRAINT FK_74ED2AA59EECAC87 FOREIGN KEY (stat_entry_item_type_id) REFERENCES stat_entry_item_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_entry_item ADD CONSTRAINT FK_74ED2AA5330198D5 FOREIGN KEY (stat_entry_id) REFERENCES stat_entry (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE stat_group');
        $this->addSql('DROP TABLE stat_type');
        $this->addSql('DROP TABLE hero_stat_entry');
        $this->addSql('DROP TABLE stat_entry_type');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2020-01-01 00:00:00+02:00\'');
        $this->addSql("INSERT INTO stat_entry_item_type (id, title, code) VALUES (1, 'Competetive', 'competitiveStats'), (2, 'Quick Play', 'quickPlayStats')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stat_entry_item DROP CONSTRAINT FK_74ED2AA59EECAC87');
        $this->addSql('DROP SEQUENCE stat_entry_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stat_entry_item_type_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE stat_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stat_entry_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stat_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hero_stat_entry_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stat_group (id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stat_type (id INT NOT NULL, stat_group_id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_ad96ac7d776308d0 ON stat_type (stat_group_id)');
        $this->addSql('CREATE TABLE hero_stat_entry (id INT NOT NULL, hero_id INT NOT NULL, stat_entry_id INT NOT NULL, stat_type_id INT NOT NULL, stat_value TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_9dbe5083330198d5 ON hero_stat_entry (stat_entry_id)');
        $this->addSql('CREATE INDEX idx_9dbe508345b0bcd ON hero_stat_entry (hero_id)');
        $this->addSql('CREATE INDEX idx_9dbe508321b6fb0a ON hero_stat_entry (stat_type_id)');
        $this->addSql('CREATE TABLE stat_entry_type (id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE stat_type ADD CONSTRAINT fk_ad96ac7d776308d0 FOREIGN KEY (stat_group_id) REFERENCES stat_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hero_stat_entry ADD CONSTRAINT fk_9dbe508345b0bcd FOREIGN KEY (hero_id) REFERENCES hero (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hero_stat_entry ADD CONSTRAINT fk_9dbe5083330198d5 FOREIGN KEY (stat_entry_id) REFERENCES stat_entry (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hero_stat_entry ADD CONSTRAINT fk_9dbe508321b6fb0a FOREIGN KEY (stat_type_id) REFERENCES stat_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE stat_entry_item');
        $this->addSql('DROP TABLE stat_entry_item_type');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2019-12-31 22:00:00+00\'');
    }
}
