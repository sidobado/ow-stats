<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200611053510 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE stat_hero_entry_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stat_hero_entry_group (id INT NOT NULL, code TEXT NOT NULL, title TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2020-01-01 00:00:00+02:00\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE stat_hero_entry_group_id_seq CASCADE');
        $this->addSql('DROP TABLE stat_hero_entry_group');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2019-12-31 22:00:00+00\'');
    }
}
