<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200611011203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stat_entry DROP CONSTRAINT fk_47064066c4cedb87');
        $this->addSql('DROP SEQUENCE player_account_id_seq CASCADE');
        $this->addSql('DROP TABLE player_account');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2020-01-01 00:00:00+02:00\'');
        $this->addSql('DROP INDEX idx_47064066c4cedb87');
        $this->addSql('ALTER TABLE stat_entry RENAME COLUMN player_account_id TO player_id');
        $this->addSql('ALTER TABLE stat_entry ADD CONSTRAINT FK_4706406699E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_4706406699E6F5DF ON stat_entry (player_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE player_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE player_account (id INT NOT NULL, player_id INT NOT NULL, endorsement INT DEFAULT NULL, endorsement_icon TEXT DEFAULT NULL, games_won INT DEFAULT NULL, icon TEXT DEFAULT NULL, level INT DEFAULT NULL, level_icon TEXT DEFAULT NULL, name TEXT DEFAULT NULL, prestige INT DEFAULT NULL, prestige_icon TEXT DEFAULT NULL, private BOOLEAN NOT NULL, rating INT DEFAULT NULL, rating_icon TEXT DEFAULT NULL, tank_level INT DEFAULT NULL, damage_level INT DEFAULT NULL, support_level INT DEFAULT NULL, tank_role_icon TEXT DEFAULT NULL, damage_role_icon TEXT DEFAULT NULL, support_role_icon TEXT DEFAULT NULL, tank_rank_icon TEXT DEFAULT NULL, damage_rank_icon TEXT DEFAULT NULL, support_rank_icon TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_1cc2a9e199e6f5df ON player_account (player_id)');
        $this->addSql('ALTER TABLE player_account ADD CONSTRAINT fk_1cc2a9e199e6f5df FOREIGN KEY (player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2019-12-31 22:00:00+00\'');
        $this->addSql('ALTER TABLE stat_entry DROP CONSTRAINT FK_4706406699E6F5DF');
        $this->addSql('DROP INDEX IDX_4706406699E6F5DF');
        $this->addSql('ALTER TABLE stat_entry RENAME COLUMN player_id TO player_account_id');
        $this->addSql('ALTER TABLE stat_entry ADD CONSTRAINT fk_47064066c4cedb87 FOREIGN KEY (player_account_id) REFERENCES player_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_47064066c4cedb87 ON stat_entry (player_account_id)');
    }
}
