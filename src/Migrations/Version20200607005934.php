<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200607005934 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE stat_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hero_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE player_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stat_entry_type_id_seq INCREMENT BY 1 MINVALUE 1 START 3');
        $this->addSql('CREATE SEQUENCE stat_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hero_stat_entry_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stat_entry_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stat_group (id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE hero (id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE player_account (id INT NOT NULL, player_id INT NOT NULL, endorsement INT DEFAULT NULL, endorsement_icon TEXT DEFAULT NULL, games_won INT DEFAULT NULL, icon TEXT DEFAULT NULL, level INT DEFAULT NULL, level_icon TEXT DEFAULT NULL, name TEXT DEFAULT NULL, prestige INT DEFAULT NULL, prestige_icon TEXT DEFAULT NULL, private BOOLEAN NOT NULL, rating INT DEFAULT NULL, rating_icon TEXT DEFAULT NULL, tank_level INT DEFAULT NULL, damage_level INT DEFAULT NULL, support_level INT DEFAULT NULL, tank_role_icon TEXT DEFAULT NULL, damage_role_icon TEXT DEFAULT NULL, support_role_icon TEXT DEFAULT NULL, tank_rank_icon TEXT DEFAULT NULL, damage_rank_icon TEXT DEFAULT NULL, support_rank_icon TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1CC2A9E199E6F5DF ON player_account (player_id)');
        $this->addSql('CREATE TABLE stat_entry_type (id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stat_type (id INT NOT NULL, stat_group_id INT NOT NULL, title TEXT NOT NULL, code TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AD96AC7D776308D0 ON stat_type (stat_group_id)');
        $this->addSql('CREATE TABLE hero_stat_entry (id INT NOT NULL, hero_id INT NOT NULL, stat_entry_id INT NOT NULL, stat_type_id INT NOT NULL, stat_value TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9DBE508345B0BCD ON hero_stat_entry (hero_id)');
        $this->addSql('CREATE INDEX IDX_9DBE5083330198D5 ON hero_stat_entry (stat_entry_id)');
        $this->addSql('CREATE INDEX IDX_9DBE508321B6FB0A ON hero_stat_entry (stat_type_id)');
        $this->addSql('CREATE TABLE stat_entry (id INT NOT NULL, player_account_id INT NOT NULL, stat_entry_type_id INT NOT NULL, hash_code TEXT NOT NULL, cards INT DEFAULT NULL, medals INT DEFAULT NULL, medals_bronze INT DEFAULT NULL, medals_silver INT DEFAULT NULL, medals_gold INT DEFAULT NULL, played INT DEFAULT NULL, won INT DEFAULT NULL, top_heroes JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_47064066C4CEDB87 ON stat_entry (player_account_id)');
        $this->addSql('CREATE INDEX IDX_47064066DF01D6EF ON stat_entry (stat_entry_type_id)');
        $this->addSql('ALTER TABLE player_account ADD CONSTRAINT FK_1CC2A9E199E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_type ADD CONSTRAINT FK_AD96AC7D776308D0 FOREIGN KEY (stat_group_id) REFERENCES stat_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hero_stat_entry ADD CONSTRAINT FK_9DBE508345B0BCD FOREIGN KEY (hero_id) REFERENCES hero (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hero_stat_entry ADD CONSTRAINT FK_9DBE5083330198D5 FOREIGN KEY (stat_entry_id) REFERENCES stat_entry (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hero_stat_entry ADD CONSTRAINT FK_9DBE508321B6FB0A FOREIGN KEY (stat_type_id) REFERENCES stat_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_entry ADD CONSTRAINT FK_47064066C4CEDB87 FOREIGN KEY (player_account_id) REFERENCES player_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_entry ADD CONSTRAINT FK_47064066DF01D6EF FOREIGN KEY (stat_entry_type_id) REFERENCES stat_entry_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2020-01-01 00:00:00+02:00\'');
        $this->addSql("INSERT INTO stat_entry_type (id, title, code) VALUES (1, 'Competetive', 'competitiveStats'), (2, 'Quick Play', 'quickPlayStats')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE stat_type DROP CONSTRAINT FK_AD96AC7D776308D0');
        $this->addSql('ALTER TABLE hero_stat_entry DROP CONSTRAINT FK_9DBE508345B0BCD');
        $this->addSql('ALTER TABLE stat_entry DROP CONSTRAINT FK_47064066C4CEDB87');
        $this->addSql('ALTER TABLE stat_entry DROP CONSTRAINT FK_47064066DF01D6EF');
        $this->addSql('ALTER TABLE hero_stat_entry DROP CONSTRAINT FK_9DBE508321B6FB0A');
        $this->addSql('ALTER TABLE hero_stat_entry DROP CONSTRAINT FK_9DBE5083330198D5');
        $this->addSql('DROP SEQUENCE stat_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hero_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE player_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stat_entry_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stat_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hero_stat_entry_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stat_entry_id_seq CASCADE');
        $this->addSql('DROP TABLE stat_group');
        $this->addSql('DROP TABLE hero');
        $this->addSql('DROP TABLE player_account');
        $this->addSql('DROP TABLE stat_entry_type');
        $this->addSql('DROP TABLE stat_type');
        $this->addSql('DROP TABLE hero_stat_entry');
        $this->addSql('DROP TABLE stat_entry');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2019-12-31 22:00:00+00\'');
    }
}
