<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200611070552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE stat_hero_entry_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stat_hero_entry_value (id INT NOT NULL, stat_entry_item_id INT NOT NULL, hero_id INT NOT NULL, stat_hero_entry_group_id INT NOT NULL, stat_hero_entry_field_type_id INT NOT NULL, value_type TEXT NOT NULL, value NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C4AFC59A8220822 ON stat_hero_entry_value (stat_entry_item_id)');
        $this->addSql('CREATE INDEX IDX_C4AFC59A45B0BCD ON stat_hero_entry_value (hero_id)');
        $this->addSql('CREATE INDEX IDX_C4AFC59A61550745 ON stat_hero_entry_value (stat_hero_entry_group_id)');
        $this->addSql('CREATE INDEX IDX_C4AFC59A6038C81B ON stat_hero_entry_value (stat_hero_entry_field_type_id)');
        $this->addSql('ALTER TABLE stat_hero_entry_value ADD CONSTRAINT FK_C4AFC59A8220822 FOREIGN KEY (stat_entry_item_id) REFERENCES stat_entry_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_hero_entry_value ADD CONSTRAINT FK_C4AFC59A45B0BCD FOREIGN KEY (hero_id) REFERENCES hero (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_hero_entry_value ADD CONSTRAINT FK_C4AFC59A61550745 FOREIGN KEY (stat_hero_entry_group_id) REFERENCES stat_hero_entry_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE stat_hero_entry_value ADD CONSTRAINT FK_C4AFC59A6038C81B FOREIGN KEY (stat_hero_entry_field_type_id) REFERENCES stat_hero_entry_field_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2020-01-01 00:00:00+02:00\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE stat_hero_entry_value_id_seq CASCADE');
        $this->addSql('DROP TABLE stat_hero_entry_value');
        $this->addSql('ALTER TABLE player ALTER last_update SET DEFAULT \'2019-12-31 22:00:00+00\'');
    }
}
