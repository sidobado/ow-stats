<?php

namespace App\Repository;

use App\Entity\StatEntryItem;
use App\Entity\StatHeroEntryValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatHeroEntryValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatHeroEntryValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatHeroEntryValue[]    findAll()
 * @method StatHeroEntryValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatHeroEntryValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatHeroEntryValue::class);
    }


    public function findByHeroAndStatType($heroId, $typeId, $fieldType, $heroType)
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.statEntryItem', 'sei')
            ->join('sei.statEntry', 'se')
            ->andWhere('s.hero = :heroId')
            ->andWhere('sei.statEntryItemType = :typeId')
            ->andWhere('s.statHeroEntryFieldType = :fieldType');

        if ($typeId == '1' && ($heroType == 'support' || $heroType == null)) {
            $qb->andWhere('se.supportLevel is not null');
        } elseif ($typeId == '1' && ($heroType == 'tank' || $heroType == null)) {
            $qb->andWhere('se.tankLevel is not null');
        } elseif ($typeId == '1' && ($heroType == 'damage' || $heroType == null)) {
            $qb->andWhere('se.damageLevel is not null');
        }

        return $qb
            ->setParameter('heroId', $heroId)
            ->setParameter('typeId', $typeId)
            ->setParameter('fieldType', $fieldType)
            ->setMaxResults(10000)
            ->getQuery()
            ->getResult()
        ;

    }


    /*
    public function findOneBySomeField($value): ?StatHeroEntryValue
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
