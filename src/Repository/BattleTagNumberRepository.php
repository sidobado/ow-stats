<?php

namespace App\Repository;

use App\Entity\BattleTagNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BattleTagNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method BattleTagNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method BattleTagNumber[]    findAll()
 * @method BattleTagNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BattleTagNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BattleTagNumber::class);
    }

    // /**
    //  * @return BattleTagNumber[] Returns an array of BattleTagNumber objects
    //  */
    public function getRandomNumber()
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.handled = false')
            ->orderBy('random()')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?BattleTagNumber
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
