<?php

namespace App\Repository;

use App\Entity\Player;
use DateInterval;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Player|null find($id, $lockMode = null, $lockVersion = null)
 * @method Player|null findOneBy(array $criteria, array $orderBy = null)
 * @method Player[]    findAll()
 * @method Player[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    // /**
    //  * @return Player[] Returns an array of Player objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Player
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return Player[]
     */
    public function findPlayersToDownloadStatistics(): array
    {
        try {
            $date = new DateTime('now', new DateTimeZone('Europe/Warsaw'));
            $date->sub(new DateInterval('P14D'));
        } catch (Exception $e) {
        }

        return $this->createQueryBuilder('b')
            ->andWhere('b.lastUpdate < :date')
            ->setParameter('date', $date->format('Y-m-d H:i:sP'))
            ->orderBy('b.lastUpdate', 'asc')
            ->addOrderBy('b.id', 'asc')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }
}
