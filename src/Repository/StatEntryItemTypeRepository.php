<?php

namespace App\Repository;

use App\Entity\StatEntryItemType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatEntryItemType|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatEntryItemType|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatEntryItemType[]    findAll()
 * @method StatEntryItemType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatEntryItemTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatEntryItemType::class);
    }

    // /**
    //  * @return StatEntryItemType[] Returns an array of StatEntryItemType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatEntryItemType
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
