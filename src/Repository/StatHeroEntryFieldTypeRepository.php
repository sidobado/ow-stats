<?php

namespace App\Repository;

use App\Entity\StatHeroEntryFieldType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatHeroEntryFieldType|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatHeroEntryFieldType|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatHeroEntryFieldType[]    findAll()
 * @method StatHeroEntryFieldType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatHeroEntryFieldTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatHeroEntryFieldType::class);
    }

    // /**
    //  * @return StatHeroEntryFieldType[] Returns an array of StatHeroEntryFieldType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatHeroEntryFieldType
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
