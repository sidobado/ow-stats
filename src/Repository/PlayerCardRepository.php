<?php

namespace App\Repository;

use App\Entity\PlayerCard;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayerCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerCard[]    findAll()
 * @method PlayerCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayerCard::class);
    }

    // /**
    //  * @return PlayerCard[] Returns an array of PlayerCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayerCard
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findCardToHandle(): ?PlayerCard
    {
        $results = $this->createQueryBuilder('b')
            ->andWhere('b.handled = false')
            ->andWhere('b.platform = :platform')
            ->setParameter('platform', 'fa fa-windows')
            ->orderBy('b.id')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;

        return $results[0] ?? null;
    }

    public function updateHandled(string $name)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        return $qb->update(PlayerCard::class, 'b')
            ->set('b.handled', ':handled')
            ->set('b.update_time', ':date')
            ->where($qb->expr()->like('lower(b.playerName)', 'lower(:name)'))
            ->setParameter('name', $name . '#%')
            ->setParameter('handled', true)
            ->setParameter('date', (new DateTime())->format('Y-m-d H:i:s'))
            ->getQuery()
            ->execute();
    }

    public function updateHandledWithoutHash(string $name)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        return $qb->update(PlayerCard::class, 'b')
            ->set('b.handled', ':handled')
            ->set('b.update_time', ':date')
            ->where('b.playerName=:name')
            ->setParameter('name', $name)
            ->setParameter('handled', true)
            ->setParameter('date', (new DateTime())->format('Y-m-d H:i:s'))
            ->getQuery()
            ->execute();
    }
}
