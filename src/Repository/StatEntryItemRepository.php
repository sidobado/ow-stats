<?php

namespace App\Repository;

use App\Entity\StatEntryItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatEntryItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatEntryItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatEntryItem[]    findAll()
 * @method StatEntryItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatEntryItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatEntryItem::class);
    }

    // /**
    //  * @return StatEntryItem[] Returns an array of StatEntryItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatEntryItem
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
