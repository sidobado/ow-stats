<?php

namespace App\Repository;

use App\Entity\StatsQueue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatsQueue|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatsQueue|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatsQueue[]    findAll()
 * @method StatsQueue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatsQueueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatsQueue::class);
    }

    // /**
    //  * @return StatsQueue[] Returns an array of StatsQueue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatsQueue
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return StatsQueue[]
     */
    public function findStatsToProcess(): array
    {
        return $this->createQueryBuilder('b')
            ->orderBy('b.id', 'asc')
            ->addOrderBy('b.id', 'asc')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }

}
