<?php

namespace App\Repository;

use App\Entity\StatEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatEntry[]    findAll()
 * @method StatEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatEntry::class);
    }

    // /**
    //  * @return StatEntry[] Returns an array of StatEntry objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatEntry
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
