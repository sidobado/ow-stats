<?php

namespace App\Repository;

use App\Entity\StatHeroEntryGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatHeroEntryGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatHeroEntryGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatHeroEntryGroup[]    findAll()
 * @method StatHeroEntryGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatHeroEntryGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatHeroEntryGroup::class);
    }

    // /**
    //  * @return StatHeroEntryGroup[] Returns an array of StatHeroEntryGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatHeroEntryGroup
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
