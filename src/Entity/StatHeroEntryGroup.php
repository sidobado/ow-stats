<?php

namespace App\Entity;

use App\Repository\StatHeroEntryGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatHeroEntryGroupRepository::class)
 */
class StatHeroEntryGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $code;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=StatHeroEntryFieldType::class, mappedBy="statHeroEntryGroup", orphanRemoval=true)
     */
    private $statHeroEntryFieldTypes;

    public function __construct()
    {
        $this->statHeroEntryFieldTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|StatHeroEntryFieldType[]
     */
    public function getStatHeroEntryFieldTypes(): Collection
    {
        return $this->statHeroEntryFieldTypes;
    }

    public function addStatHeroEntryFieldType(StatHeroEntryFieldType $statHeroEntryFieldType): self
    {
        if (!$this->statHeroEntryFieldTypes->contains($statHeroEntryFieldType)) {
            $this->statHeroEntryFieldTypes[] = $statHeroEntryFieldType;
            $statHeroEntryFieldType->setStatHeroEntryGroup($this);
        }

        return $this;
    }

    public function removeStatHeroEntryFieldType(StatHeroEntryFieldType $statHeroEntryFieldType): self
    {
        if ($this->statHeroEntryFieldTypes->contains($statHeroEntryFieldType)) {
            $this->statHeroEntryFieldTypes->removeElement($statHeroEntryFieldType);
            // set the owning side to null (unless already changed)
            if ($statHeroEntryFieldType->getStatHeroEntryGroup() === $this) {
                $statHeroEntryFieldType->setStatHeroEntryGroup(null);
            }
        }

        return $this;
    }
}
