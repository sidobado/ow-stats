<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $urlName;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="boolean")
     */
    private $public;

    /**
     * @ORM\Column(type="text")
     */
    private $platform;

    /**
     * @ORM\Column(type="text")
     */
    private $portrait;

    /**
     * @ORM\Column(type="datetimetz", options={"default": "2020-01-01 00:00:00+02:00"})
     */
    private $lastUpdate;

    /**
     * @ORM\OneToMany(targetEntity=StatEntry::class, mappedBy="player", orphanRemoval=true)
     */
    private $statEntries;

    public function __construct()
    {
        $this->lastUpdate = new DateTime("2020-01-01 00:00:00+02:00");
        $this->statEntries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrlName(): ?string
    {
        return $this->urlName;
    }

    public function setUrlName(string $urlName): self
    {
        $this->urlName = $urlName;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getPortrait(): ?string
    {
        return $this->portrait;
    }

    public function setPortrait(string $portrait): self
    {
        $this->portrait = $portrait;

        return $this;
    }

    public function getLastUpdate(): DateTimeInterface
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(DateTimeInterface $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * @return Collection|StatEntry[]
     */
    public function getStatEntries(): Collection
    {
        return $this->statEntries;
    }

    public function addStatEntry(StatEntry $statEntry): self
    {
        if (!$this->statEntries->contains($statEntry)) {
            $this->statEntries[] = $statEntry;
            $statEntry->setPlayer($this);
        }

        return $this;
    }

    public function removeStatEntry(StatEntry $statEntry): self
    {
        if ($this->statEntries->contains($statEntry)) {
            $this->statEntries->removeElement($statEntry);
            // set the owning side to null (unless already changed)
            if ($statEntry->getPlayer() === $this) {
                $statEntry->setPlayer(null);
            }
        }

        return $this;
    }
}
