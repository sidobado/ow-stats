<?php

namespace App\Entity;

use App\Repository\StatHeroEntryFieldTypeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatHeroEntryFieldTypeRepository::class)
 */
class StatHeroEntryFieldType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=StatHeroEntryGroup::class, inversedBy="statHeroEntryFieldTypes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statHeroEntryGroup;

    /**
     * @ORM\Column(type="text")
     */
    private $valueType;

    /**
     * @ORM\Column(type="text")
     */
    private $code;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatHeroEntryGroup(): ?StatHeroEntryGroup
    {
        return $this->statHeroEntryGroup;
    }

    public function setStatHeroEntryGroup(?StatHeroEntryGroup $statHeroEntryGroup): self
    {
        $this->statHeroEntryGroup = $statHeroEntryGroup;

        return $this;
    }

    public function getValueType(): ?string
    {
        return $this->valueType;
    }

    public function setValueType(string $valueType): self
    {
        $this->valueType = $valueType;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
