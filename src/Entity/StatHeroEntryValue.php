<?php

namespace App\Entity;

use App\Repository\StatHeroEntryValueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatHeroEntryValueRepository::class)
 */
class StatHeroEntryValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=StatEntryItem::class, inversedBy="statHeroEntryValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statEntryItem;

    /**
     * @ORM\ManyToOne(targetEntity=Hero::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $hero;

    /**
     * @ORM\ManyToOne(targetEntity=StatHeroEntryGroup::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $statHeroEntryGroup;

    /**
     * @ORM\ManyToOne(targetEntity=StatHeroEntryFieldType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $statHeroEntryFieldType;

    /**
     * @ORM\Column(type="text")
     */
    private $valueType;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatEntryItem(): ?StatEntryItem
    {
        return $this->statEntryItem;
    }

    public function setStatEntryItem(?StatEntryItem $statEntryItem): self
    {
        $this->statEntryItem = $statEntryItem;

        return $this;
    }

    public function getHero(): ?Hero
    {
        return $this->hero;
    }

    public function setHero(?Hero $hero): self
    {
        $this->hero = $hero;

        return $this;
    }

    public function getStatHeroEntryGroup(): ?StatHeroEntryGroup
    {
        return $this->statHeroEntryGroup;
    }

    public function setStatHeroEntryGroup(?StatHeroEntryGroup $statHeroEntryGroup): self
    {
        $this->statHeroEntryGroup = $statHeroEntryGroup;

        return $this;
    }

    public function getStatHeroEntryFieldType(): ?StatHeroEntryFieldType
    {
        return $this->statHeroEntryFieldType;
    }

    public function setStatHeroEntryFieldType(?StatHeroEntryFieldType $statHeroEntryFieldType): self
    {
        $this->statHeroEntryFieldType = $statHeroEntryFieldType;

        return $this;
    }

    public function getValueType(): ?string
    {
        return $this->valueType;
    }

    public function setValueType(string $valueType): self
    {
        $this->valueType = $valueType;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
