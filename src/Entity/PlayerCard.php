<?php

namespace App\Entity;

use App\Repository\PlayerCardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlayerCardRepository::class)
 */
class PlayerCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $playerLevel;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $playerIcon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $playerName;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $lastUpdate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $platform;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sr;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $topHeroes = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $outdated;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $create_time;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $update_time;

    /**
     * @ORM\Column(type="integer")
     */
    private $version_number;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $handled = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayerLevel(): ?int
    {
        return $this->playerLevel;
    }

    public function setPlayerLevel(?int $playerLevel): self
    {
        $this->playerLevel = $playerLevel;

        return $this;
    }

    public function getPlayerIcon(): ?string
    {
        return $this->playerIcon;
    }

    public function setPlayerIcon(?string $playerIcon): self
    {
        $this->playerIcon = $playerIcon;

        return $this;
    }

    public function getPlayerName(): ?string
    {
        return $this->playerName;
    }

    public function setPlayerName(?string $playerName): self
    {
        $this->playerName = $playerName;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(?\DateTimeInterface $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(?string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getSr(): ?int
    {
        return $this->sr;
    }

    public function setSr(?int $sr): self
    {
        $this->sr = $sr;

        return $this;
    }

    public function getTopHeroes(): ?array
    {
        return $this->topHeroes;
    }

    public function setTopHeroes(?array $topHeroes): self
    {
        $this->topHeroes = $topHeroes;

        return $this;
    }

    public function addTopHero(string $hero): self
    {
        $this->topHeroes[] = $hero;

        return $this;
    }

    public function getOutdated(): ?bool
    {
        return $this->outdated;
    }

    public function setOutdated(?bool $outdated): self
    {
        $this->outdated = $outdated;

        return $this;
    }

    public function getCreateTime(): ?\DateTimeInterface
    {
        return $this->create_time;
    }

    public function setCreateTime(\DateTimeInterface $create_time): self
    {
        $this->create_time = $create_time;

        return $this;
    }

    public function getUpdateTime(): ?\DateTimeInterface
    {
        return $this->update_time;
    }

    public function setUpdateTime(\DateTimeInterface $update_time): self
    {
        $this->update_time = $update_time;

        return $this;
    }

    public function getVersionNumber(): ?int
    {
        return $this->version_number;
    }

    public function setVersionNumber(int $version_number): self
    {
        $this->version_number = $version_number;

        return $this;
    }

    public function isHandled(): bool
    {
        return $this->handled;
    }

    public function setHandled(bool $handled): self
    {
        $this->handled = $handled;

        return $this;
    }
}
