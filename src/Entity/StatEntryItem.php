<?php

namespace App\Entity;

use App\Repository\StatEntryItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatEntryItemRepository::class)
 */
class StatEntryItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=StatEntryItemType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $statEntryItemType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cards;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $medals;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $medalsBronze;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $medalsSilver;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $medalsGold;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $played;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $won;

    /**
     * @ORM\ManyToOne(targetEntity=StatEntry::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $statEntry;

    /**
     * @ORM\OneToMany(targetEntity=StatHeroEntryValue::class, mappedBy="statEntryItem", orphanRemoval=true)
     */
    private $statHeroEntryValues;

    public function __construct()
    {
        $this->statHeroEntryValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatEntryItemType(): ?StatEntryItemType
    {
        return $this->statEntryItemType;
    }

    public function setStatEntryItemType(?StatEntryItemType $statEntryItemType): self
    {
        $this->statEntryItemType = $statEntryItemType;

        return $this;
    }

    public function getCards(): ?int
    {
        return $this->cards;
    }

    public function setCards(?int $cards): self
    {
        $this->cards = $cards;

        return $this;
    }

    public function getMedals(): ?int
    {
        return $this->medals;
    }

    public function setMedals(?int $medals): self
    {
        $this->medals = $medals;

        return $this;
    }

    public function getMedalsBronze(): ?int
    {
        return $this->medalsBronze;
    }

    public function setMedalsBronze(?int $medalsBronze): self
    {
        $this->medalsBronze = $medalsBronze;

        return $this;
    }

    public function getMedalsSilver(): ?int
    {
        return $this->medalsSilver;
    }

    public function setMedalsSilver(?int $medalsSilver): self
    {
        $this->medalsSilver = $medalsSilver;

        return $this;
    }

    public function getMedalsGold(): ?int
    {
        return $this->medalsGold;
    }

    public function setMedalsGold(?int $medalsGold): self
    {
        $this->medalsGold = $medalsGold;

        return $this;
    }

    public function getPlayed(): ?int
    {
        return $this->played;
    }

    public function setPlayed(?int $played): self
    {
        $this->played = $played;

        return $this;
    }

    public function getWon(): ?int
    {
        return $this->won;
    }

    public function setWon(?int $won): self
    {
        $this->won = $won;

        return $this;
    }

    public function getStatEntry(): ?StatEntry
    {
        return $this->statEntry;
    }

    public function setStatEntry(?StatEntry $statEntry): self
    {
        $this->statEntry = $statEntry;

        return $this;
    }

    /**
     * @return Collection|StatHeroEntryValue[]
     */
    public function getStatHeroEntryValues(): Collection
    {
        return $this->statHeroEntryValues;
    }

    public function addStatHeroEntryValue(StatHeroEntryValue $statHeroEntryValue): self
    {
        if (!$this->statHeroEntryValues->contains($statHeroEntryValue)) {
            $this->statHeroEntryValues[] = $statHeroEntryValue;
            $statHeroEntryValue->setStatEntryItem($this);
        }

        return $this;
    }

    public function removeStatHeroEntryValue(StatHeroEntryValue $statHeroEntryValue): self
    {
        if ($this->statHeroEntryValues->contains($statHeroEntryValue)) {
            $this->statHeroEntryValues->removeElement($statHeroEntryValue);
            // set the owning side to null (unless already changed)
            if ($statHeroEntryValue->getStatEntryItem() === $this) {
                $statHeroEntryValue->setStatEntryItem(null);
            }
        }

        return $this;
    }
}
