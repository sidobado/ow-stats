<?php

namespace App\Entity;

use App\Repository\StatEntryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatEntryRepository::class)
 */
class StatEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $hashCode;

    /**
     * @ORM\Column(type="text")
     */
    private $competitiveStatsHashCode;

    /**
     * @ORM\Column(type="text")
     */
    private $quickPlayStatsHashCode;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $endorsement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $endorsementIcon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $gamesWon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $icon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $level;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $levelIcon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prestige;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $prestigeIcon;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $private;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ratingIcon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tankLevel;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tankRoleIcon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tankRankIcon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $damageLevel;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $damageRoleIcon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $damageRankIcon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $supportLevel;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $supportRoleIcon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $supportRankIcon;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="statEntries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;


    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHashCode(): ?string
    {
        return $this->hashCode;
    }

    public function setHashCode(string $hashCode): self
    {
        $this->hashCode = $hashCode;

        return $this;
    }

    public function getCompetitiveStatsHashCode(): ?string
    {
        return $this->competitiveStatsHashCode;
    }

    public function setCompetitiveStatsHashCode(string $hashCode): self
    {
        $this->competitiveStatsHashCode = $hashCode;

        return $this;
    }

    public function getQuickPlayStatsHashCode(): ?string
    {
        return $this->quickPlayStatsHashCode;
    }

    public function setQuickPlayStatsHashCode(string $hashCode): self
    {
        $this->quickPlayStatsHashCode = $hashCode;

        return $this;
    }

    public function getEndorsement(): ?int
    {
        return $this->endorsement;
    }

    public function setEndorsement(?int $endorsement): self
    {
        $this->endorsement = $endorsement;

        return $this;
    }

    public function getEndorsementIcon(): ?string
    {
        return $this->endorsementIcon;
    }

    public function setEndorsementIcon(?string $endorsementIcon): self
    {
        $this->endorsementIcon = $endorsementIcon;

        return $this;
    }

    public function getGamesWon(): ?int
    {
        return $this->gamesWon;
    }

    public function setGamesWon(?int $gamesWon): self
    {
        $this->gamesWon = $gamesWon;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getLevelIcon(): ?string
    {
        return $this->levelIcon;
    }

    public function setLevelIcon(?string $levelIcon): self
    {
        $this->levelIcon = $levelIcon;

        return $this;
    }

    public function getPrestige(): ?int
    {
        return $this->prestige;
    }

    public function setPrestige(?int $prestige): self
    {
        $this->prestige = $prestige;

        return $this;
    }

    public function getPrestigeIcon(): ?string
    {
        return $this->prestigeIcon;
    }

    public function setPrestigeIcon(?string $prestigeIcon): self
    {
        $this->prestigeIcon = $prestigeIcon;

        return $this;
    }

    public function getPrivate(): bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): self
    {
        $this->private = $private;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getRatingIcon(): ?string
    {
        return $this->ratingIcon;
    }

    public function setRatingIcon(?string $ratingIcon): self
    {
        $this->ratingIcon = $ratingIcon;

        return $this;
    }

    public function getTankLevel(): ?int
    {
        return $this->tankLevel;
    }

    public function setTankLevel(?int $tankLevel): self
    {
        $this->tankLevel = $tankLevel;

        return $this;
    }

    public function getTankRoleIcon(): ?string
    {
        return $this->tankRoleIcon;
    }

    public function setTankRoleIcon(?string $tankRoleIcon): self
    {
        $this->tankRoleIcon = $tankRoleIcon;

        return $this;
    }

    public function getTankRankIcon(): ?string
    {
        return $this->tankRankIcon;
    }

    public function setTankRankIcon(?string $tankRankIcon): self
    {
        $this->tankRankIcon = $tankRankIcon;

        return $this;
    }

    public function getDamageLevel(): ?int
    {
        return $this->damageLevel;
    }

    public function setDamageLevel(?int $damageLevel): self
    {
        $this->damageLevel = $damageLevel;

        return $this;
    }

    public function getDamageRoleIcon(): ?string
    {
        return $this->damageRoleIcon;
    }

    public function setDamageRoleIcon(?string $damageRoleIcon): self
    {
        $this->damageRoleIcon = $damageRoleIcon;

        return $this;
    }

    public function getDamageRankIcon(): ?string
    {
        return $this->damageRankIcon;
    }

    public function setDamageRankIcon(?string $damageRankIcon): self
    {
        $this->damageRankIcon = $damageRankIcon;

        return $this;
    }

    public function getSupportLevel(): ?int
    {
        return $this->supportLevel;
    }

    public function setSupportLevel(?int $supportLevel): self
    {
        $this->supportLevel = $supportLevel;

        return $this;
    }

    public function getSupportRoleIcon(): ?string
    {
        return $this->supportRoleIcon;
    }

    public function setSupportRoleIcon(?string $supportRoleIcon): self
    {
        $this->supportRoleIcon = $supportRoleIcon;

        return $this;
    }

    public function getSupportRankIcon(): ?string
    {
        return $this->supportRankIcon;
    }

    public function setSupportRankIcon(?string $supportRankIcon): self
    {
        $this->supportRankIcon = $supportRankIcon;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

}
